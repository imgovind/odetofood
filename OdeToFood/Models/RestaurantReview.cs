﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OdeToFood.Models
{
    public class RestaurantReview : IValidatableObject
    {
        public int Id { get; set; }
        [StringLength(1024)]
        //[Required(ErrorMessageResourceType=typeof(OdeToFood.Views.Home.Resources), ErrorMessageResourceName="ErrorMessage")]
        [Required]
        public string Body { get; set; }
        [Range(0, 10)]
        [Required]
        public int Rating { get; set; }
        public int RestaurantId { get; set; }
        [Display(Name = "User Name")]
        [DisplayFormat(NullDisplayText = "anonymous")]
        public string ReviewerName { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (RestaurantId == 1 && Rating < 8)
            {
                yield return new ValidationResult("Sorry, you cannot give such a low rating for Saravana Bhavan. Its the best.");
            }
        }
    }
}