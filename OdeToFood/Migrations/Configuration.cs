namespace OdeToFood.Migrations
{
    using OdeToFood.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<OdeToFood.Models.OdeToFoodDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OdeToFood.Models.OdeToFoodDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Restaurants.AddOrUpdate(
                p => p.Name,
                new Restaurant { Name="Saravana Bhavan", City="Chennai", Country = "India" },
                new Restaurant { Name="Mathura", City = "Chennai", Country = "India" },
                new Restaurant { Name="Mari", City="Chennai", Country="India", 
                    Reviews = new List<RestaurantReview> { 
                        new RestaurantReview{ Rating = 10, Body = "Vada kari is super", ReviewerName = "Govindarajan Panneerselvam" }
                    } }
                );

            SeedMembership();
        }

        private void SeedMembership()
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("OdeToFoodDbConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            }
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membersip = (SimpleMembershipProvider)Membership.Provider;
            if (!roles.RoleExists("Admin")) 
            {
                roles.CreateRole("Admin");
            }
            if (membersip.GetUser("valar", false) == null)
            {
                membersip.CreateUserAndAccount("valar", "valaru");
            }
            if (!roles.GetRolesForUser("valar").Contains("Admin")) 
            {
                roles.AddUsersToRoles(new[] { "valar" }, new[] { "Admin" });
            }
        }
    }
}
