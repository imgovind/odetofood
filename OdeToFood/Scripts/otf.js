﻿$(function () {

    var ajaxFormSubmit = function () {
        var $form = $(this);

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-otf-target"));
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            //$newHtml.addClass('animated fadeInUp');
            setAnimate($newHtml)
            //$newHtml.effect('highlight');
        });

        return false;
    }

    var searchAnimate = function (element) {
        var $elementChildren = element.children();
        $.each($elementChildren, function (index, val) {
            var $elementChild = $(val);
            if (index > 0) {
                $elementChild.addClass('animated fadeInUp');
            }
        });
    };

    var submitAutocompleteForm = function (event, ui) {
        var $input = $(this);
        $input.val(ui.item.label);

        var $form = $input.parents("form:first");
        $form.submit();

    };

    var createAutocomplete = function () {
        var $input = $(this);

        var options = {
            source: $input.attr("data-otf-autocomplete"),
            select: submitAutocompleteForm
        };

        $input.autocomplete(options);
    };

    var pageAnimate = function (element, anchor) {
        var $elementChildren = element.children();
        $.each($elementChildren, function (index, val) {
            var $elementChild = $(val);
            if (index > 0) {
                if (anchor.parent().attr("class") == "PagedList-skipToPrevious") {
                    $elementChild.addClass('animated fadeInRight');
                } else {
                    $elementChild.addClass('animated fadeInLeft');
                }
            }
        });
    };

    var setAnimate = function (element, anchor) {
        var $elementChildren = element.children();
        $.each($elementChildren, function (index, val) {
            if (index > 0)
            {
                var $elementChild = $(val);
                if (typeof anchor != 'undefined') {
                    if (anchor.parent().attr("class") == "PagedList-skipToPrevious") {
                        $elementChild.addClass('animated fadeInRight');
                    } else {
                        $elementChild.addClass('animated fadeInLeft');
                    }
                } else {
                    $elementChild.addClass('animated fadeInUp');
                }
            }
        });
    }

    var getPage = function () {
        var $a = $(this);

        var options = {
            url: $a.attr("href"),
            data: $("form").serialize(),
            type: "get"
        };

        $.ajax(options).done(function (data) {
            var target = $a.parents("div.pagedList").attr("data-otf-target");
            var $newHtml = $(data);
            $(target).replaceWith($newHtml);
            setAnimate($newHtml, $a);
        });
        return false;
    };

    $("form[data-otf-ajax='true']").submit(ajaxFormSubmit);

    $("input[data-otf-autocomplete]").each(createAutocomplete);

    $(".main-content").on("click", ".pagedList a", getPage);
})